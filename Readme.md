
# BigRedButton for OS X

## Examples

 * terminal-notifier
```bash
./BigRedButton /usr/local/bin/terminal-notifier -title "Big Red Button" -message "BOOM\!"
```

 * screenbrightness
```bash
./BigRedButton /bin/bash -c "screenbrightness 0; sleep 0.5s; screenbrightness 0.8"
```

 * screen inverting
```bash
./BigRedButton /usr/bin/osascript -e 'tell application "System Events" to key code 28 using {control down, option down, command down}'
```
 * Pop up
```bash
./BigRedButton /usr/bin/osascript -e 'tell app "System Events" to display dialog "Boom"'
```

## Links

https://github.com/gef3233/red_button/blob/master/button.c
https://github.com/dustice/emergencybutton/blob/master/emergencybutton.c
http://thelumpenproletarian.blogspot.com/2013/12/usb-panic-button-linux-script.html
http://stackoverflow.com/questions/20600349/get-callback-using-iokit-using-an-interrupt-input-endpoint
https://github.com/derrick/dream_cheeky/blob/master/ext/dream_cheeky/dream_cheeky_big_red_button.c
