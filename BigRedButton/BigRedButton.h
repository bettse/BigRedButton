//
//  BigRedButton.h
//  BigRedButton
//
//  Created by Eric Betts on 12/9/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IOKit/hid/IOHIDLib.h>

#define COMMAND_SIZE 8
#define STATUS_INTERVAL 0.3
#define GET_STATUS_BYTE 7
#define GET_STATUS_VALUE 0x02
#define STATUS_BYTE 0
#define BUTTON_PRESSED 0x01
#define COVER_OPEN 0x02

//0x15 0b10101
//0x16 0b10110
//0x17 0b10111

@interface BigRedButton : NSObject {
    
}

@property IOHIDDeviceRef button;
-(void) initUsb;

+(BigRedButton*) getInstance;
@property NSTimer *timer;

@end