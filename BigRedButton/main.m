//
//  main.m
//  BigRedButton
//  http://files.dreamcheeky.com.s3.amazonaws.com/uploads/dc/902/BigRedButtonDevManual.pdf
//
//  Created by Eric Betts on 1/24/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BigRedButton.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSArray *arguments = [[NSProcessInfo processInfo] arguments];
        if (arguments.count < 2) {
          arguments = @[@"/usr/bin/say", @"Boom"];
        } else {
          arguments = [arguments subarrayWithRange:(NSRange){1, arguments.count - 1}];
        }
        NSLog(@"Command to run: %@", [arguments componentsJoinedByString:@" "]);

        //Spin off daemon into its own thread
        BigRedButton *button = [BigRedButton getInstance];
        NSThread* daemon = [[NSThread alloc] initWithTarget:button selector:@selector(initUsb) object:nil];
        [daemon start];

        
        [[NSNotificationCenter defaultCenter] addObserverForName:@"deviceConnected" object:nil queue:nil usingBlock:^(NSNotification *notification)
         {
             NSLog(@"deviceConnected");
         }];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:@"buttonPressed" object:nil queue:nil usingBlock:^(NSNotification *notification)
         {

             NSTask *task = [NSTask launchedTaskWithLaunchPath:arguments[0] arguments:[arguments subarrayWithRange:(NSRange){1, arguments.count - 1}]];
             [task waitUntilExit];
             int status = [task terminationStatus];
             NSLog(@"Task termination status: %i.", status);
         }];
        [[NSRunLoop currentRunLoop] run];
    }
    return 0;
}
