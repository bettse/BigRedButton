//
//  BigRedButton.m
//  BigRedButton
//
//  Created by Eric Betts on 12/9/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import "BigRedButton.h"

@implementation BigRedButton

static BigRedButton* singleton;

+(BigRedButton*) getInstance
{
    if (!singleton){
        singleton = [[self alloc] init];
    }
    return singleton;
}

- (void) getStatus {
    Byte bytes[COMMAND_SIZE] = {0};
    bytes[GET_STATUS_BYTE] = GET_STATUS_VALUE;
    NSData *command = [NSData dataWithBytes:bytes length:COMMAND_SIZE];
    long reportSize = 0;
    if (self.button) {
        //NSLog(@"%s %@", __PRETTY_FUNCTION__, command);
        IOHIDDeviceSetReport(self.button, kIOHIDReportTypeOutput, reportSize, (uint8_t *)[command bytes], [command length]);
    }
}

void inputCallback(void *inContext, IOReturn result, void *sender, IOHIDReportType type, uint32_t reportID, uint8_t *report, CFIndex reportLength)
{
    BigRedButton* self = (__bridge BigRedButton*) inContext;
    NSData *data = [NSData dataWithBytes:report length:reportLength];
    [self parseAck:data];
    //NSLog(@"%s: %@", __FUNCTION__, data);
}

-(void) parseAck:(NSData*)data {
    const uint8_t *bytes = [data bytes];
    Byte status = bytes[STATUS_BYTE];
    //NSLog(@"%s %X, %X", __PRETTY_FUNCTION__, status, status & BUTTON_PRESSED);
    if ((status & BUTTON_PRESSED) == 0) {//Button uses inverse logic
        [[NSNotificationCenter defaultCenter] postNotificationName: @"buttonPressed" object:nil userInfo:nil];
    }
    if ((status & COVER_OPEN) == COVER_OPEN) {
        [[NSNotificationCenter defaultCenter] postNotificationName: @"coverOpen" object:nil userInfo:nil];
    }
}

//http://developer.apple.com/library/mac/#documentation/DeviceDrivers/Conceptual/HID/new_api_10_5/tn2187.html
// function to get a long device property
// returns FALSE if the property isn't found or can't be converted to a long
static Boolean IOHIDDevice_GetLongProperty(IOHIDDeviceRef inDeviceRef, CFStringRef inKey, long * outValue)
{
    Boolean result = FALSE;
    CFTypeRef tCFTypeRef = IOHIDDeviceGetProperty(inDeviceRef, inKey);
    if (tCFTypeRef) {
        // if this is a number
        if (CFNumberGetTypeID() == CFGetTypeID(tCFTypeRef)) {
            // get its value
            result = CFNumberGetValue((CFNumberRef) tCFTypeRef, kCFNumberSInt32Type, outValue);
        }
    }
    return result;
}

// this will be called when the HID Manager matches a new (hot plugged) HID device
static void Handle_DeviceMatchingCallback(void* inContext, IOReturn inResult, void* inSender, IOHIDDeviceRef inIOHIDDeviceRef)
{
    @autoreleasepool {
        long reportSize = 0;
        BigRedButton* self = (__bridge BigRedButton*) inContext;
        self.button = inIOHIDDeviceRef;
        uint8_t *report;
        (void)IOHIDDevice_GetLongProperty(inIOHIDDeviceRef, CFSTR(kIOHIDMaxInputReportSizeKey), &reportSize);
        if (reportSize) {
            report = calloc(1, reportSize);
            if (report) {
                IOHIDDeviceRegisterInputReportCallback(inIOHIDDeviceRef, report, reportSize, inputCallback, inContext);
                NSDictionary *userInfo = @{@"class": NSStringFromClass([self class])};
                //NSLog(@"%s %@", __PRETTY_FUNCTION__, userInfo);
                [[NSNotificationCenter defaultCenter] postNotificationName: @"deviceConnected" object:nil userInfo:userInfo];
                self.timer = [NSTimer scheduledTimerWithTimeInterval:STATUS_INTERVAL target:self selector:@selector(getStatus) userInfo:nil repeats:YES];
            }
        }
    }
}

// this will be called when a HID device is removed (unplugged)
static void Handle_DeviceRemovalCallback(void* inContext, IOReturn inResult, void* inSender, IOHIDDeviceRef inIOHIDDeviceRef)
{
    @autoreleasepool {
        BigRedButton* self = (__bridge BigRedButton*) inContext;
        self.button = nil;
        [self.timer invalidate];
        self.timer = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName: @"deviceDisconnected" object:nil userInfo:@{@"class": NSStringFromClass([self class])}];
    }
}

-(void) initUsb
{
    @autoreleasepool {
        const long vendorId = 0x1D34;
        const long productId = 0x000D;
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        IOHIDManagerRef managerRef = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDOptionsTypeNone);
        IOHIDManagerScheduleWithRunLoop(managerRef, CFRunLoopGetMain(), kCFRunLoopDefaultMode);
        IOHIDManagerOpen(managerRef, 0L);
        dict[@kIOHIDProductIDKey] = @(productId);
        dict[@kIOHIDVendorIDKey] = @(vendorId);
        IOHIDManagerSetDeviceMatching(managerRef, (__bridge CFMutableDictionaryRef)dict);
        IOHIDManagerRegisterDeviceMatchingCallback(managerRef, Handle_DeviceMatchingCallback, (__bridge void *)(self));
        IOHIDManagerRegisterDeviceRemovalCallback(managerRef, Handle_DeviceRemovalCallback, (__bridge void *)(self));

        //NSLog(@"Starting runloop");
        [[NSRunLoop currentRunLoop] run];
    }
}


@end

